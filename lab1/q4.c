#include <stdio.h>
struct pkt
{
    unsigned char ch1;
    unsigned char ch2[2];
    unsigned char ch3;
};
int main()
{
    int num = 0xDFD6C9E5;
    printf("Number: %X\n", num);
    struct pkt bytes;
    bytes.ch1 = (unsigned char)num;
    bytes.ch2[0] = (unsigned char)(num >> 8);
    bytes.ch2[1] = (unsigned char)(num >> 16);
    bytes.ch3 = (unsigned char)(num >> 24);
    printf("ch1: %X\n", bytes.ch1);
    printf("ch2[0]: %X\n", bytes.ch2[0]);
    printf("ch2[1]: %X\n", bytes.ch2[1]);
    printf("ch3: %X\n", bytes.ch3);
    int agg_num = (int)bytes.ch1;
    agg_num |= (int)bytes.ch2[0] << 8;
    agg_num |= (int)bytes.ch2[1] << 16;
    agg_num |= (int)bytes.ch3 << 24;
    printf("Aggregated Number: %X\n", agg_num);
}