#include <stdio.h>
int main()
{
    int num = 0xDFD5D7D9;
    printf("Number: %X\n", num);
    unsigned char bytes[4];
    unsigned char *char_ptr = (unsigned char *)&num;
    for (int i = 0; i < 4; ++i)
    {
        bytes[i] = *char_ptr;
        char_ptr++;
    }
    for(int i = 0; i < 4; ++i)
    {
        printf("Byte %d: %X\n", i + 1, bytes[i]);
    }
}