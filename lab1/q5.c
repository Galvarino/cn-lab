#include <stdio.h>
int main()
{
    int num;
    printf("Enter a number: ");
    scanf("%d", &num);
    if (num == 0)
    {
        num++;
    }
    if ((unsigned char)num == 0)
    {
        printf("Big Endian: 0x%X\n", num);
        printf("Converted to Little Endian: 0x");
    }
    else
    {
        printf("Little Endian: 0x%X\n", num);
        printf("Converted to Big Endian: 0x");
    }
    int temp_num = (int)(unsigned char)num;
    temp_num <<= 8;
    temp_num |= (unsigned char)(num >> 8);
    temp_num <<= 8;
    temp_num |= (unsigned char)(num >> 16);
    temp_num <<= 8;
    temp_num |= (unsigned char)(num >> 24);
    printf("%X\n", temp_num);
}