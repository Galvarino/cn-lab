#include <stdio.h>
#include <string.h>
struct Date
{
    int day;
    int month;
    int year;
};
struct Student_Info
{
    int roll_no;
    char name[20];
    float cgpa;
    struct Date DOB;
};
void print_by_value(struct Student_Info stu)
{
    printf("\nPassed by value\n");
    printf("Roll Number: %d\n", stu.roll_no);
    printf("Name: %s\n", stu.name);
    printf("CGPA: %f\n", stu.cgpa);
    printf("Date of Birth: %d-%d-%d\n", stu.DOB.day, stu.DOB.month, stu.DOB.year);
}
void print_by_address(struct Student_Info *stu)
{
    printf("\nPassed by address\n");
    printf("Roll Number: %d\n", stu->roll_no);
    printf("Name: %s\n", stu->name);
    printf("CGPA: %f\n", stu->cgpa);
    printf("Date of Birth: %d-%d-%d\n", stu->DOB.day, stu->DOB.month, stu->DOB.year);
}
int main()
{
    struct Student_Info stu;
    stu.roll_no = 33;
    strcpy(stu.name, "Raman");
    stu.cgpa = 9.99;
    stu.DOB.day = 22;
    stu.DOB.month = 7;
    stu.DOB.year = 2002;
    print_by_value(stu);
    print_by_address(&stu);
}