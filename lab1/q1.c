#include <stdio.h>
void swap(int *var1, int *var2)
{
    int temp = *var1;
    *var1 = *var2;
    *var2 = temp;
}
int main()
{
    int var1, var2;
    printf("Variable 1: ");
    scanf("%d", &var1);
    printf("Variable 2: ");
    scanf("%d", &var2);
    swap(&var1, &var2);
    printf("\nVariable 1 now: %d\n", var1);
    printf("Variable 2 now: %d\n", var2);
}