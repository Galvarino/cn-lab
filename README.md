# CN Lab

## Lab 1

1. WACP to swap the contents of two variables entered through the command line using function and pointer.

2. Write a C program to assign values to each members of the following
structure. Pass the populated structure to a function Using call-by-value and another function using call-by-address and print the value of each member of the structure.
struct student_info{
int roll_no;
char name[20];
float CGPA;
struct dob age;
};

3. Write a C program to extract each byte from a given number and store them in separate character variables and print the content of those variables.

4. Write a C Program to enter a number and store the number across the
following structure and print the content of each member of the structure.
Then aggregate each member of the structure to form the original number and
print the same.
struct pkt{
char ch1;
char ch2[2];
char ch3;
};

5. Write a C program to check whether the Host machine is in Little Endian or Big Endian. Enter a number, print the content of each byte location and Convert the Endianness of the same i.e. Little to Big Endian and vice-versa.


## Lab 2

1. Write a sender and receiver program in C by passing the IP address and the port number of each other.
2. Write a sender and receiver program in C by passing the IP address and the port number of each other (Bind function). 


## Lab 3

1. Write a sender and receiver program in C by passing the IP address and the port number of each other through the command line arguments using connection less socket. Both of them will exchange messages with each other continuously. If any one of them will receive the “exit” message from the other end then both of them will close the connection. (Assume both the client and server are running with in the same host)
2. Write a sender and receiver program in C using connection less socket that client will send a integer and server will find out the Factorial of that number and send the result to client.
3. Write a sender and receiver program in C using connection less socket that client will send an array to server and get the sorted array as a response.
4. Write a sender and receiver program in C using connection less socket that client will send a structure array of 5 students and server will find out the total mark and Avg mark to the client.

## Lab 4 (Test)

## Lab 5
(TCP) - Write a sender and receiver program in C using connection oriented socket that client will send a message and server will receive and vice-versa.

## Lab 6
(server_io_multiplexing and client_io_multiplexing) - 
Write a sender and receiver program that sends a 'hi client' message to every client concurrently.

## Lab 7
Cisco Packet connection using switch, show all topologies (mesh, bus, ring, star, hybrid)

## Lab 8
Cisco Packet using Router
