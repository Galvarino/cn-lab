#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
int main()
{
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd == -1)
    {
        printf("Failed to create the socket...\n");
        exit(0);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8080);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t len = sizeof(struct sockaddr_in);
    int bd = bind(sock_fd, (const struct sockaddr *)&server_addr, len);
    if (bd == -1)
    {
        printf("Failed to bind to the port...\n");
        exit(0);
    }
    if (listen(sock_fd, 5) == -1)
    {
        printf("Failed to listen...\n");
        exit(0);
    }
    struct sockaddr_in client_addr;
    int conn_fd = accept(sock_fd, (struct sockaddr *)&client_addr, &len);
    if (conn_fd == -1)
    {
        printf("Failed to accept...\n");
        exit(0);
    }
    char buff[100];
    strcpy(buff, "init");
    while (strcmp(buff, "exit"))
    {
        read(conn_fd, buff, sizeof(buff));
        printf("Received: %s\n", buff);
        if (strcmp(buff, "exit"))
        {
            printf("Message for the client: ");
            scanf("%99[^\n]", buff);
            char c;
            while ((c = getchar()) != '\n' && c != EOF)
                ;
            write(conn_fd, buff, sizeof(buff));
        }
    }
    close(sock_fd);
}