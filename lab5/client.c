#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
int main()
{
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd == -1)
    {
        printf("Failed to create the socket...\n");
        exit(0);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8080);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t len = sizeof(struct sockaddr_in);
    int conn_fd = connect(sock_fd, (const struct sockaddr *)&server_addr, len);
    if (conn_fd == -1)
    {
        printf("Failed to connect...\n");
        exit(0);
    }
    char buff[100];
    strcpy(buff, "init");
    while (strcmp(buff, "exit"))
    {
        printf("Message for the client: ");
        scanf("%99[^\n]", buff);
        char c;
        while ((c = getchar()) != '\n' && c != EOF)
            ;
        write(sock_fd, buff, sizeof(buff));
        if (strcmp(buff, "exit"))
        {
            read(sock_fd, buff, sizeof(buff));
            printf("Received: %s\n", buff);
        }
    }
    close(sock_fd);
}