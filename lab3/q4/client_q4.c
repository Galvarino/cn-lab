#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
struct Student
{
    int roll_no;
    float marks;
};
int main()
{
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1)
    {
        printf("Error in creating socket...\n");
        exit(1);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t addr_len = sizeof(struct sockaddr_in);
    struct Student s[5];
    s[0].roll_no = 33;
    s[0].marks = 67;
    s[1].roll_no = 56;
    s[1].marks = 45;
    s[2].roll_no = 26;
    s[2].marks = 95;
    s[3].roll_no = 77;
    s[3].marks = 58;
    s[4].roll_no = 31;
    s[4].marks = 72;
    sendto(sock_fd, s, sizeof(s), 0, (const struct sockaddr *)&server_addr, addr_len);
    float tot, avg;
    recvfrom(sock_fd, &tot, sizeof(tot), 0, (struct sockaddr *)&server_addr, &addr_len);
    printf("Total: %0.2f\n", tot);
    recvfrom(sock_fd, &avg, sizeof(avg), 0, (struct sockaddr *)&server_addr, &addr_len);
    printf("Average: %0.2f\n", avg);
    close(sock_fd);
}