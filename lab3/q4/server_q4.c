#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
struct Student
{
    int roll_no;
    float marks;
};
int main()
{
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1)
    {
        printf("Error in creating socket...\n");
        exit(1);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t addr_len = sizeof(struct sockaddr_in);
    int bd = bind(sock_fd, (const struct sockaddr *)&server_addr, addr_len);
    if (bd == -1)
    {
        printf("Error in binding...\n");
        exit(1);
    }
    struct Student s[5];
    struct sockaddr_in client_addr;
    recvfrom(sock_fd, s, sizeof(s), 0, (struct sockaddr *)&client_addr, &addr_len);
    float tot = 0;
    for (int i = 0; i < 5; ++i)
    {
        tot += s[i].marks;
    }
    float avg = tot / 5;
    sendto(sock_fd, &tot, sizeof(tot), 0, (const struct sockaddr *)&client_addr, addr_len);
    sendto(sock_fd, &avg, sizeof(avg), 0, (const struct sockaddr *)&client_addr, addr_len);
    close(sock_fd);
}