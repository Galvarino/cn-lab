#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
int main()
{
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1)
    {
        printf("Error in creating socket...\n");
        exit(1);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t addr_len = sizeof(struct sockaddr_in);
    int num = 5;
    sendto(sock_fd, &num, sizeof(num), 0, (const struct sockaddr *)&server_addr, addr_len);
    long long int fact;
    recvfrom(sock_fd, &fact, sizeof(fact), 0, (struct sockaddr *)&server_addr, &addr_len);
    printf("Factorial: %lld\n", fact);
    close(sock_fd);
}