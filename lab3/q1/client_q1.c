#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
int main(int no_of_args, char **args)
{
    if (no_of_args != 3)
    {
        printf("Expected two arguments\n");
        exit(1);
    }
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1)
    {
        printf("Error in creating socket...\n");
        exit(1);
    }
    printf("IP: %s\n", args[1]);
    printf("Port: %d\n", atoi(args[2]));
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(atoi(args[2]));
    server_addr.sin_addr.s_addr = inet_addr(args[1]);
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t addr_len = sizeof(struct sockaddr_in);
    char buff[100];
    strcpy(buff, "init");
    while (strcmp(buff, "exit"))
    {
        printf("Message for the server: ");
        scanf("%99[^\n]", buff);
        sendto(sock_fd, buff, sizeof(buff), 0, (const struct sockaddr *)&server_addr, addr_len);
        char c;
        while ((c = getchar()) != '\n' && c != EOF)
            ;
        if (strcmp(buff, "exit"))
        {
            recvfrom(sock_fd, buff, sizeof(buff), 0, (struct sockaddr *)&server_addr, &addr_len);
            printf("Received: %s\n", buff);
        }
    }
    close(sock_fd);
}
