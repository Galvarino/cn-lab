#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#define ARR_SIZE 5
int main()
{
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1)
    {
        printf("Error in creating socket...\n");
        exit(1);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t addr_len = sizeof(struct sockaddr_in);
    int arr[ARR_SIZE] = {5, 34, 55, 10, 23};
    sendto(sock_fd, arr, sizeof(arr), 0, (const struct sockaddr *)&server_addr, addr_len);
    int sorted_arr[5];
    recvfrom(sock_fd, sorted_arr, sizeof(sorted_arr), 0, (struct sockaddr *)&server_addr, &addr_len);
    printf("Sorted Array: ");
    for (int i = 0; i < ARR_SIZE; ++i)
    {
        printf(" %d", sorted_arr[i]);
    }
    printf("\n");
    close(sock_fd);
}