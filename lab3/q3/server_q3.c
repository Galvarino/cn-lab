#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#define ARR_SIZE 5
void sort(int *arr)
{
    for (int i = 0; i < ARR_SIZE - 1; ++i)
    {
        for (int j = 0; j < ARR_SIZE - i - 1; ++j)
        {
            if (arr[j] > arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}
int main()
{
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd == -1)
    {
        printf("Error in creating socket...\n");
        exit(1);
    }
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_addr.sin_zero, 0, sizeof(server_addr.sin_zero));
    socklen_t addr_len = sizeof(struct sockaddr_in);
    int bd = bind(sock_fd, (const struct sockaddr *)&server_addr, addr_len);
    if (bd == -1)
    {
        printf("Error in binding...\n");
        exit(1);
    }
    int arr[ARR_SIZE];
    struct sockaddr_in client_addr;
    recvfrom(sock_fd, arr, sizeof(arr), 0, (struct sockaddr *)&client_addr, &addr_len);
    printf("Received Array: ");
    for (int i = 0; i < ARR_SIZE; ++i)
    {
        printf(" %d", arr[i]);
    }
    sort(arr);
    printf("\nSorted Array: ");
    for (int i = 0; i < ARR_SIZE; ++i)
    {
        printf(" %d", arr[i]);
    }
    printf("\n");
    sendto(sock_fd, arr, sizeof(arr), 0, (const struct sockaddr *)&client_addr, addr_len);
    close(sock_fd);
}